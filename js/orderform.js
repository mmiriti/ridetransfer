function initOrderForm(basePath) {
	$(function() {
		$('#find-from-input').bind('keyup', function() {
			if ($(this).val().length > 2) {
				$.ajax({
					url: basePath + '/fetchlocations/?q=' + $('#find-from-input').val(),
					dataType: 'json',
					error: function() {},
					success: function(jData) {
						if ((jData != undefined) && (jData.items != undefined)) {
							$('#find-from-container').html('');

							$.each(jData.items, function(index, value) {
								$('<a>', {
									href: '#',
									text: value['title'],
									click: function() {
										$('#find-form').hide();
										$('#find-from-label').html(value['title']);
										$('#find-from-final-block').show();
										$('#route-from-id').val(value['id']);

										$.ajax({
											url: basePath + '/findroute/?location_from=' + value['id'],
											dataType: 'json',
											success: function(jData) {
												$('#info-block-from .info-label').html(jData['from_type']['title']);
												$('#info-block-from').show();

												$('#route-to-id').html('');
												$('#route-to-id').removeAttr('disabled');
												$('#route-to-id').append('<option>- выбирите -</option>');

												$.each(jData['list_to'], function(routeId, routeTitle) {
													$('<option>', {
														value: routeId,
														text: routeTitle
													}).appendTo('#route-to-id');
												});
											}
										});
									}
								}).appendTo($('#find-from-container'));
							});
						}
					}
				});
			} else {
				$('#find-from-container').html('');
			}
		});

		$('#route-to-id').bind('change', function() {
			var locationFromID = $('#route-from-id').val();
			var locationToID = $('#route-to-id').val();

			$('#submit-button').attr('disabled', 'true');

			$.ajax({
				url: basePath + '/autoclass?from=' + locationFromID + '&to=' + locationToID,
				dataType: 'json',
				success: function(jData) {
					$('#info-block-to .info-label').html(jData['to_type']['title']);
					$('#info-block-to').show();

					$('#route-auto-class').removeAttr('disabled');
					$('#route-auto-class').html('');
					$('#route-auto-class').append('<option>- выбирите класс -</option>');

					$.each(jData['classes'], function(routeID, classTitle) {
						$('<option>', {
							value: routeID,
							text: classTitle
						}).appendTo('#route-auto-class');
					});
				}
			});
		});

		$('#route-auto-class').bind('change', function() {
			var routeID = $(this).val();

			$.ajax({
				url: basePath + '/route/id/' + routeID,
				dataType: 'json',
				success: function(jData) {
					$('#order-price').removeAttr('disabled');
					$('#order-price').val(jData['price']);

					$('#passanger-count').html('');

					for (var i = 1; i <= jData['seat_count']; i++) {
						$('<option>', {
							value: i,
							text: i
						}).appendTo('#passanger-count');
					};

					$('#passanger-count').removeAttr('disabled');

					$('#passanger-count').val(jData['seat_count']);

					$('#big-price').html(jData['price']);
					$('#submit-button').removeAttr('disabled');
				}
			});
		});

		$('.passanger-name-input').blur(function() {
			$('#input-sign-text').val($('#input-passanger-first-name').val() + ' ' + $('#input-passanger-last-name').val());
		});
	});
};