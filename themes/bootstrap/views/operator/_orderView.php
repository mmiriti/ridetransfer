<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="order-modal-label-<?php echo $order->id; ?>">Просмотр заказа №<?php echo $order->id; ?></h4>
		</div>
		<div class="modal-body">
			<div class="panel panel-default">
				<div class="panel-heading">Клиент</div>
				<div class="panel-body">
					<?php echo $order->client->title; ?>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Дата и время создания</div>
				<div class="panel-body">
					<?php echo date("d.m.Y H:i", strtotime($order->create_time)); ?>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Дата и время подачи автомобиля</div>
				<div class="panel-body"><?php echo date("d.m.Y H:i", strtotime($order->ride_time)); ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Пассажир</div>
				<div class="panel-body"><?php echo $order->passenger->fullname; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Примечание</div>
				<div class="panel-body"><?php echo $order->comment; ?></div>
			</div>			

			<div class="panel panel-default">
				<div class="panel-heading">Пункт отправления</div>
				<div class="panel-body"><?php echo $order->route->locationFrom->title; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading"><?php echo $order->route->locationFrom->locationType->typeInfo->title; ?></div>
				<div class="panel-body"><?php echo $order->info_from; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Пункт назначения</div>
				<div class="panel-body"><?php echo $order->route->locationTo->title; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading"><?php echo $order->route->locationTo->locationType->typeInfo->title; ?></div>
				<div class="panel-body"><?php echo $order->info_to; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Класс автомобиля</div>
				<div class="panel-body"><?php echo $order->route->autoClass->title; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Количество пассажиров</div>
				<div class="panel-body"><?php echo $order->passanger_count; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Надпись на табличке</div>
				<div class="panel-body"><?php echo $order->sign_text; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Стоимость</div>
				<div class="panel-body"><?php echo $order->price; ?></div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Статус заказа</div>
				<div class="panel-body"><?php echo $order->orderStatus->title; ?></div>
			</div>

		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
		</div>
	</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->