<script type="text/javascript">
	jQuery(function(){
		console.log('redy');
		$('.modal').modal();
	});
</script>
<div class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Вход для корпоративных клиентов</h4>
			</div>
			<div class="modal-body">
				<?php echo CHtml::beginForm(array('user/login'), 'post', array('role' => 'form')); ?>
				<div class="form-group">
					<label for="inputLogin">Логин</label>
					<input type="login" name="UserLogin[login]" class="form-control" id="inputLogin" placeholder="">
				</div>
				<div class="form-group">
					<label for="inputPassword">Пароль</label>
					<input type="password" name="UserLogin[password]" class="form-control" id="inputPassword" placeholder="">
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="UserLogin[remember]"> Запомнить меня
					</label>
				</div>
				<button type="submit" class="btn btn-primary">Войти</button>
				<?php echo CHtml::endForm(); ?>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->