<div class="row">
	<div class="col-md-12">
		<?php echo CHtml::link('<span class="glyphicon glyphicon-plus"></span> Создать заказ', array('client/index', 'action' => 'create'), array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php 

foreach ($ordersList as $order) { ?>
<div class="modal fade" id="order-modal-<?php echo $order->id; ?>" tabindex="-1" role="dialog" aria-labelledby="order-modal-label-<?php echo $order->id; ?>" aria-hidden="true">
</div>
<?php
}

$this->widget('application.components.TableList', array(
	'listModel' => Order::model(),
	'listData' => $ordersList,
	'excludeFields' => array('client_id', 'route_id', 'info_from', 'info_to', 'passanger_count', 'comment', 'sign_text', 'deleted'),
	'fieldTypes' => array(
		'create_time' => array(
			'type' => 'date_format',
			'format' => 'd/m/Y H:i'
		),
		'ride_time' => array(
			'type' => 'date_format',
			'format' => 'd/m/Y H:i'
		),
		'passenger_id' => array(
			'type' => 'related',
			'rel' => 'passenger',
			'field' => 'fullname'
		),
		'location_from_id' => array(
			'type' => 'related',
			'rel' => 'locationFrom',
			'field' => 'title'
		),
		'location_to_id' => array(
			'type' => 'related',
			'rel' => 'locationTo',
			'field' => 'title'
		),
		'auto_class_id' => array(
			'type' => 'related',
			'rel' => 'autoClass',
			'field' => 'title'
		),
		'order_status_id' => array(
			'type' => 'related',
			'rel' => 'orderStatus',
			'field' => 'title'
		)
	),
	'action' => 'client/index',
	'showButtons' => array(),
	'additionalButtons' => array(
		array(
			'icon_class' => 'glyphicon-eye-open',
			'action' => array('index', 'action' => 'view'),
			'title' => 'Просмотр',
			'btn_class' => 'btn-success',
			'htmlOptions' => array('data-toggle' => 'modal', 'data-target' => '#order-modal-%id%')
		)
	)
));

$this->widget('application.components.ListSummary', array(
	'list' => $ordersList
	));
