<div class="row">
	<div class="col-md-12">
		<?php echo CHtml::link('Создать клиента', array('admin/clients', 'action' => 'create'), array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php $this->widget('application.components.TableList', array(
	'listModel' => Client::model(),
	'excludeFields' => array('id'),
	'fieldTypes' => array(
		'user_id' => array(
			'type' => 'related',
			'rel' => 'user',
			'field' => 'login'
		)
	),
	'action' => 'admin/clients'
)); ?>