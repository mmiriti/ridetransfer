<div class="row">
	<div class="col-md-12">
		<?php echo CHtml::link('Создать класс', array('admin/classes', 'action' => 'create'), array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php $this->widget('application.components.TableList', array(
	'listModel' => AutoClass::model(),
	'excludeFields' => array('id'),
	'fieldTypes' => array(),
	'action' => 'admin/classes'
)); ?>