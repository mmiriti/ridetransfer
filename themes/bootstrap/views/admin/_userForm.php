<?php 
echo CHtml::link('Назад', array('admin/users'), array('class' => 'btn btn-default'));
$this->widget('application.components.BootstrapForm', array(
	'model' => $user,
	'fieldTypes' => array(
		'pwd_hash' => 'password',
		'user_access_id' => array(
			'type' => 'select',
			'listData' => CHtml::listData(UserAccess::model()->findAll(), 'id', 'title')
		)
	)
)); 
?>