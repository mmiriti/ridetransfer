<?php 
echo CHtml::link('Назад', array('admin/passengers'), array('class' => 'btn btn-default'));
$this->widget('application.components.BootstrapForm', array(
	'model' => $passenger,
	'fieldTypes' => array(
		'client_id' => array(
			'type' => 'select',
			'listData' => CHtml::listData(Client::model()->findAll(), 'id', 'title')
		),
		'memo' => 'textarea'
	)
	)); 
?>