<div class="row">
	<div class="col-md-12">
		<?php echo CHtml::link('Создать статус', array('admin/statuses', 'action' => 'create'), array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php $this->widget('application.components.TableList', array(
	'listModel' => OrderStatus::model(),
	'excludeFields' => array('id'),
	'fieldTypes' => array(),
	'action' => 'admin/statuses'
)); ?>