<div class="row">
	<div class="col-md-12">
		<?php echo CHtml::link('Создать пасажира', array('admin/passengers', 'action' => 'create'), array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php $this->widget('application.components.TableList', array(
	'listModel' => Passenger::model(),
	'excludeFields' => array('id'),
	'fieldTypes' => array(
		'client_id' => array(
			'type' => 'related',
			'rel' => 'client',
			'field' => 'title'
		)
	),
	'action' => 'admin/passengers'
)); ?>