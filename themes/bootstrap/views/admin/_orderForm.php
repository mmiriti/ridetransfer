<script type="text/javascript" src="/js/orderform.js"></script>
<script type="text/javascript">
	initOrderForm('<?php echo Yii::app()->request->baseUrl; ?>/admin');
</script>
<style>
	#find-from-container a {
		display: block;
	}
</style>
<?php 
echo CHtml::link('<span class="glyphicon glyphicon-circle-arrow-left"></span> Назад', array('admin/index'), array('class' => 'btn btn-default'));
?>
<div class="row">
	<div class="col-md-9">
		<form class="form-horizontal" role="form" action="" method="post" >
		<?php

		if(count($errors)) 
		{ 
			foreach ($errors as $error_block) 
			{
				foreach ($error_block as $field_name => $error) 
				{
		?>
		<div class="alert alert-danger"><?php echo $error; ?></div>
		<?php
				}
			}
		}

		?>
			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Клиент</label>
				<div class="col-sm-10">
					<?php echo CHtml::activeDropDownList($order, 'client_id', CHtml::listData(Client::model()->findAll(), 'id', 'title'), array('class' => 'form-control')); ?>
				</div>
			</div>
			<div class="form-group" id="find-from-final-block"<?php if($order->route == null) { ?> style="display: none;"<?php } ?>>
				<label for="" class="col-sm-2 control-label">Откуда</label>
				<div class="col-sm-10">
					<input type="hidden" name="Route[from]" id="route-from-id" value="<?php if($order->route != null) { echo $order->route->location_from; } ?>">
					<p class="form-control-static" id="find-from-label"><?php echo $order->route == null ? '' : $order->route->locationFrom->title . ' (' . $order->route->locationFrom->locationType->name . ')'; ?></p>
				</div>
			</div>
			
			<?php if($order->route == null) { ?>
			<span id="find-form">
				<div class="form-group" id="find-from-block">
					<label for="" class="col-sm-2 control-label">Откуда</label>
					<div class="col-sm-10">
						<input type="text" name="Route[from]" class="form-control location_type" value="" id="find-from-input" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-sm-2 control-label"></label>
					<div class="col-sm-10" id="find-from-container">
						
					</div>
				</div>
			</span>
			<?php } ?>

			<div class="form-group" id="info-block-from"<?php if($order->route == null){ ?> style="display:none;"<?php } ?>>
				<label for="" class="col-sm-2 control-label info-label"><?php echo $order->route == null ? '' : $order->route->locationTo->locationType->typeInfo->title; ?></label>
				<div class="col-sm-10">
					<?php 
						echo CHtml::activeTextArea($order, 'info_from', array('class' => 'form-control')); 
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Куда</label>
				<div class="col-sm-10">					
					<?php
					$data = array();

					if($order->route != null)
					{
						$routes = Route::model()->findAllByAttributes(array('location_from' => $order->route->location_from));

						foreach ($routes as $r) 
						{
							$data[$r->location_to] = $r->locationTo->title . ' (' . $r->locationTo->locationType->name . ')';
						}
					}

					echo CHtml::dropDownList('Route[to]', $order->route == null ? 0 : $order->route->location_to, $data, array('class' => 'form-control', 'id' => 'route-to-id', 'disabled' => ($order->route == null)));
					?>
					</select>
				</div>
			</div>

			<div class="form-group" id="info-block-to"<?php if($order->route == null){ ?> style="display:none;"<?php } ?>>
				<label for="" class="col-sm-2 control-label info-label"><?php echo $order->route == null ? '' : $order->route->locationTo->locationType->typeInfo->title; ?></label>
				<div class="col-sm-10">
					<?php 
						echo CHtml::activeTextArea($order, 'info_to', array('class' => 'form-control')); 
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Класс автомобиля</label>
				<div class="col-sm-10">
					<?php 
					if($order->route != null)
					{
						$disabled = false;
					} else {
						$disabled = true;
					} 

					echo CHtml::dropDownList('Order[route_id]', $order == null ? 0 : $order->route_id, $autoClassList, array('class' => 'form-control', 'id' => 'route-auto-class', 'disabled' => $disabled)); //$order->route->autoClass->title; 
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Количество пассажиров</label>
				<div class="col-sm-10">
					<?php
						$data = array();

						if($order->route != null)
						{
							for ($i=1; $i <= intval($order->route->autoClass->seat_count); $i++) { 
								$data[$i] = $i;
							}
							$disabled = false;
						}else{
							$disabled = true;
						}

						echo CHtml::activeDropDownList($order, 'passanger_count', $data, array('id' => 'passanger-count', 'class' => 'form-control', 'disabled' => $disabled));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Стоимость поездки</label>
				<div class="col-sm-10">
					<?php echo CHtml::activeTextField($order, 'price', array('class' => 'form-control', 'id' => 'order-price', 'disabled' => ($order->route == null))); ?>
				</div>
			</div>


			<!-- date + time -->
			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Дата поездки</label>
				<div class="col-sm-10">
					<input type="date" name="Order[ride_time][date]" class="form-control" value="<?php echo date("Y-m-d", strtotime($order->ride_time)); ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Время подачи автомобиля</label>
				<div class="col-sm-10">
					<input type="time" name="Order[ride_time][time]" class="form-control" value="<?php echo date("H:i:s", strtotime($order->ride_time)); ?>">
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Имя пассажира</label>
				<div class="col-sm-10">
					<?php 
						if(isset($passenger))
						{
							echo CHtml::activeTextField($passenger, 'first_name', array('class' => 'form-control passanger-name-input', 'id' => 'input-passanger-first-name')); 
						}else
						{
							echo CHtml::activeTextField($order->passenger, 'first_name', array('class' => 'form-control passanger-name-input', 'id' => 'input-passanger-first-name')); 
						}						
					?>
				</div>
			</div>
			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Фамилия пассажира</label>
				<div class="col-sm-10">
					<?php 
						if(isset($passenger))
						{
							echo CHtml::activeTextField($passenger, 'last_name', array('class' => 'form-control passanger-name-input', 'id' => 'input-passanger-last-name')); 
						}else
						{
							echo CHtml::activeTextField($order->passenger, 'last_name', array('class' => 'form-control passanger-name-input', 'id' => 'input-passanger-last-name')); 
						}						
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Номер телефона пассажира</label>
				<div class="col-sm-10">
					<?php 
						if(isset($passenger))
						{
							echo CHtml::activeTextField($passenger, 'phone', array('class' => 'form-control')); 
						}else
						{
							echo CHtml::activeTextField($order->passenger, 'phone', array('class' => 'form-control')); 
						}						
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">e-mail пассажира</label>
				<div class="col-sm-10">
					<?php 
						if(isset($passenger))
						{
							echo CHtml::activeTextField($passenger, 'email', array('class' => 'form-control')); 
						}else
						{
							echo CHtml::activeTextField($order->passenger, 'email', array('class' => 'form-control')); 
						}						
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Примечание пассажира</label>
				<div class="col-sm-10">
					<?php echo CHtml::activeTextArea($order, 'comment', array('class' => 'form-control', 'id' => 'passanger-memo')); ?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Надпись на табличке</label>
				<div class="col-sm-10">
					<?php
						echo CHtml::activeTextField($order, 'sign_text', array('class' => 'form-control', 'id' => 'input-sign-text'));
					?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label">Статус</label>
				<div class="col-sm-10">
					<?php echo CHtml::activeDropDownList($order, 'order_status_id', CHtml::listData(OrderStatus::model()->findAll(), 'id', 'title'), array('class' => 'form-control')); ?>
				</div>
			</div>

			<div class="form-group">
				<label for="" class="col-sm-2 control-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-primary" id="submit-button"<?php if($order->route == null){ ?> disabled="true"<?php } ?>>Сохранить</button>
				</div>
			</div>

		</form>
	</div>
	<div class="col-md-3">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3>Стоимость поездки:</h3>
			</div>
			<div class="panel-body">			
				<h1><span id="big-price"><?php echo $order->price; ?></span>р</h1>
			</div>
		</div>
	</div>
</div>