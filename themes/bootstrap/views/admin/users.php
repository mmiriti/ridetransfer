<div class="row">
	<div class="col-md-12">
		<?php echo CHtml::link('Создать пользователя', array('admin/users', 'action' => 'create'), array('class' => 'btn btn-primary')); ?>
	</div>
</div>
<?php $this->widget('application.components.TableList', array(
	'listModel' => User::model(),
	'excludeFields' => array('id', 'pwd_hash'),
	'fieldTypes' => array('user_access_id' => array(
		'type' => 'related', 
		'rel' => 'userAccess', 
		'field' => 'title'
	)),
	'action' => 'admin/users'
)); ?>