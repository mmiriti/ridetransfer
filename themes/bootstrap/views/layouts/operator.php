<!DOCTYPE html>
<html>
<head>
	<title><?php echo $this->pageTitle; ?></title>
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Bootstrap -->
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://code.jquery.com/jquery.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
	<div class="navbar navbar-inverse" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<?php echo CHtml::link('Оператор', array('operator/index'), array('class' => 'navbar-brand')); ?>
			</div>
			<div class="collapse navbar-collapse">
				<?php 
				$this->widget('zii.widgets.CMenu', array(
					'items'=>array(
						array('label' => 'Заказы', 'url' => array('operator/index')),
						array('label' => 'Выход [' . Yii::app()->user->role . ']', 'url' => array('user/logout'))
						),
					'htmlOptions' => array('class' => 'nav navbar-nav')
					)); ?>
				</div><!--/.nav-collapse -->
			</div>
		</div>

		<div class="container">
			<?php echo $content; ?>
		</div>
	</body>
	</html>