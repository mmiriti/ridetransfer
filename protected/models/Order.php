<?php

/**
 * This is the model class for table "rdt_order".
 *
 * The followings are the available columns in table 'rdt_order':
 * @property integer $id
 * @property string $create_time
 * @property string $ride_time
 * @property integer $client_id
 * @property integer $passenger_id
 * @property integer $route_id
 * @property string $comment
 * @property string $info_from
 * @property string $info_to
 * @property integer $passanger_count
 * @property string $sign_text
 * @property double $price
 * @property integer $order_status_id
 * @property integer $deleted
 *
 * The followings are the available model relations:
 * @property Passenger $passenger
 * @property OrderStatus $orderStatus
 * @property Route $route
 * @property Client $client
 */
class Order extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rdt_order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id, passenger_id, route_id, order_status_id', 'required'),
			array('client_id, passenger_id, route_id, passanger_count, order_status_id, deleted', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			array('info_from, info_to', 'length', 'max'=>255),
			array('sign_text', 'length', 'max'=>256),
			array('create_time, ride_time, comment', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, create_time, ride_time, client_id, passenger_id, route_id, comment, info_from, info_to, passanger_count, sign_text, price, order_status_id, deleted', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'passenger' => array(self::BELONGS_TO, 'Passenger', 'passenger_id'),
			'orderStatus' => array(self::BELONGS_TO, 'OrderStatus', 'order_status_id'),
			'route' => array(self::BELONGS_TO, 'Route', 'route_id'),
			'client' => array(self::BELONGS_TO, 'Client', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => '№ Заказа',
			'create_time' => 'Дата и время создания',
			'ride_time' => 'Дата и время поездки',
			'client_id' => 'Клиент',
			'passenger_id' => 'Пассажир',
			'route_id' => 'Маршрут',
			'comment' => 'Комментарий',
			'info_from' => 'Info From',
			'info_to' => 'Info To',
			'passanger_count' => 'Количество пассажиров',
			'sign_text' => 'Надпись на табличке',
			'price' => 'Стоимость',
			'order_status_id' => 'Статус',
			'deleted' => 'Флаг удаления',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('ride_time',$this->ride_time,true);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('passenger_id',$this->passenger_id);
		$criteria->compare('route_id',$this->route_id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('info_from',$this->info_from,true);
		$criteria->compare('info_to',$this->info_to,true);
		$criteria->compare('passanger_count',$this->passanger_count);
		$criteria->compare('sign_text',$this->sign_text,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('order_status_id',$this->order_status_id);
		$criteria->compare('deleted',$this->deleted);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
