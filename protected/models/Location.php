<?php

/**
 * This is the model class for table "rdt_location".
 *
 * The followings are the available columns in table 'rdt_location':
 * @property integer $id
 * @property string $title
 * @property integer $city_id
 * @property integer $location_type_id
 *
 * The followings are the available model relations:
 * @property City $city
 * @property LocationType $locationType
 * @property Route[] $routes
 * @property Route[] $routes1
 */
class Location extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rdt_location';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location_type_id', 'required'),
			array('city_id, location_type_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, city_id, location_type_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'city' => array(self::BELONGS_TO, 'City', 'city_id'),
			'locationType' => array(self::BELONGS_TO, 'LocationType', 'location_type_id'),
			'routes' => array(self::HAS_MANY, 'Route', 'location_from'),
			'routes1' => array(self::HAS_MANY, 'Route', 'location_to'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Локация',
			'city_id' => 'City',
			'location_type_id' => 'Location Type',
		);
	}

	public function getFulltitle()
	{
		$result = $this->title;

		if($this->city != null)
		{
			if($this->city->region != null)
			{
				if($this->city->region->country != null)
				{
					$result = $this->city->region->country->name . ', ' . $this->city->region->name . ', ' . $this->city->name . ', ' . $this->title . ' (' . $this->locationType->name . ')';
				}else
				{
					$result = $this->city->region->name . ', ' . $this->city->name . ', ' . $this->title . ' (' . $this->locationType->name . ')';
				}
			}else
			{
				$result = $this->city->name . ', ' . $this->title . ' (' . $this->locationType->name . ')';
			}
		}else
		{
			$result = $this->title . ' (' . $this->locationType->name . ')';
		}

		return $result;
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('location_type_id',$this->location_type_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Location the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
