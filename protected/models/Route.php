<?php

/**
 * This is the model class for table "rdt_route".
 *
 * The followings are the available columns in table 'rdt_route':
 * @property integer $id
 * @property integer $location_from
 * @property integer $location_to
 * @property integer $auto_class_id
 * @property double $price
 *
 * The followings are the available model relations:
 * @property Order[] $orders
 * @property AutoClass $autoClass
 * @property Location $locationFrom
 * @property Location $locationTo
 */
class Route extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rdt_route';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('location_from, location_to, auto_class_id', 'required'),
			array('location_from, location_to, auto_class_id', 'numerical', 'integerOnly'=>true),
			array('price', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, location_from, location_to, auto_class_id, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'orders' => array(self::HAS_MANY, 'Order', 'route_id'),
			'autoClass' => array(self::BELONGS_TO, 'AutoClass', 'auto_class_id'),
			'locationFrom' => array(self::BELONGS_TO, 'Location', 'location_from'),
			'locationTo' => array(self::BELONGS_TO, 'Location', 'location_to', 'order' => 'title asc'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'location_from' => 'Location From',
			'location_to' => 'Location To',
			'auto_class_id' => 'Auto Class',
			'price' => 'Price',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('location_from',$this->location_from);
		$criteria->compare('location_to',$this->location_to);
		$criteria->compare('auto_class_id',$this->auto_class_id);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Route the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
