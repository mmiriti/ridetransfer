<?php

/**
* 
*/
class TableList extends CWidget
{
	public $positionField = FALSE;
	public $listModel;
	public $fieldList;
	public $fieldTypes = array();
	public $listData;
	public $excludeFields = array('id');
	public $additionalButtons = array();
	public $action = 'index';
	public $showButtons = array('edit', 'delete');

	public function run() {
		$this->render('tableList');
	}
}