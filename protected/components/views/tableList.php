<form action="position" method="post">
	<table class="table table-stripped table-hover">
		<?php
		$resultFieldList = array();

		if($this->listModel != null)
		{
			$attributeLabels = $this->listModel->attributeLabels();

			?>
			<thead>
				<tr>
					<?php
					if($this->positionField !== FALSE) 
					{
						?>
						<th>#</th>
						<?php
					}

					if(is_array($this->fieldList))
					{
						foreach ($this->fieldList as $fieldName) 
						{
							$resultFieldList[] = $fieldName;
							echo '<th>' . $attributeLabels[$fieldName] . '<th>';
						}
					}else{
						foreach ($attributeLabels as $fieldName => $attributeLabel) 
						{
							if(!in_array($fieldName, $this->excludeFields))
							{
								$resultFieldList[] = $fieldName;
								echo '<th>' . $attributeLabel . '</th>';
							}
						}
					}
					?>
					<th></th>
				</tr>
			</thead>
			<?php
		}
		?>
		<tbody>
			<?php
			if(!is_array($this->listData)) 
			{
				$this->listData = $this->listModel->findAll();
			}

			if(count($this->listData) > 0) 
			{
				foreach ($this->listData as $listRow) 
				{
					?>
					<tr>
					<?php
					if($this->positionField !== FALSE) 
					{
						?>
					<td><input type="text" class="span1" value="<?php echo $listRow[$this->positionField]; ?>" name="Position[<?php echo $listRow->id; ?>]"></td>
						<?php
					}

					foreach ($resultFieldList as $colName) 
					{
						$value = $listRow[$colName];

						echo '<td>';

						if(array_key_exists($colName, $this->fieldTypes))
						{
							if(is_array($this->fieldTypes[$colName]))
							{								
								switch ($this->fieldTypes[$colName]['type']) {
									case 'related':
										echo $listRow[$this->fieldTypes[$colName]['rel']][$this->fieldTypes[$colName]['field']];
										break;

									case 'date_format':
										$timestamp = strtotime($value);
										$format = $this->fieldTypes[$colName]['format'];
										echo date($format, $timestamp);
										break;
									
									default:
										break;
								}
							}else{
								switch ($this->fieldTypes[$colName]) {
									case 'image_file_id':
										$imageFile = File::model()->findByPk(intval($value));

										echo '<a href="' . $imageFile->src . '"><img src="' . $imageFile->src . '" style="max-height: 100px;"></a>';

										break;								
								}
							}
						}else{
							echo $value;
						}

						echo '</td>';
					}
					?>
					<td>
						<div class="btn-group">
							<?php 								
								if(in_array('edit', $this->showButtons))
								{
									echo CHtml::link('<span class="glyphicon glyphicon-pencil"></span>', array($this->action, 'action' => 'update', 'id' => $listRow->id), array('class' => 'btn btn-default btn-sm', 'title' => 'Редактировать'));
								}

								if(in_array('delete', $this->showButtons))
								{
									echo CHtml::link('<span class="glyphicon glyphicon-trash"></span>', array($this->action, 'action' => 'delete', 'id' => $listRow->id), array('class' => 'btn btn-sm btn-danger', 'title' => 'Удалить', 'onclick' => "return confirm('Вы уверены?');"));
								}
							
								foreach ($this->additionalButtons as $addButton) 
								{
									foreach ($addButton['htmlOptions'] as $option => $value) 
									{
										$addButton['htmlOptions'][$option] = str_replace('%id%', $listRow->id, $value);
									}
									
									echo CHtml::link('<span class="glyphicon ' . $addButton['icon_class'] . '"></span>', is_array($addButton['action']) ? array_merge($addButton['action'], array('id' => $listRow->id)) : str_replace('%id%', $listRow->id, $addButton['action']), array_merge($addButton['htmlOptions'], array('class' => 'btn btn-sm ' . $addButton['btn_class'], 'title' => $addButton['title'])));
								} 
							?>
						</div>
					</td>
					<?php
					echo '</tr>';
				}
			} else
			{
				?>
				<tr>
					<td colspan="<?php echo (count($resultFieldList)+1); ?>"><p class="text-center">- нет записей -</p></td>
				</tr>
				<?php
			}

			if($this->positionField !== FALSE) 
			{
			?>
			<tr>
				<td colspan="<?php echo count($resultFieldList) + 2; ?>"><button type="submit" class="btn btn-primary">Save</button></td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</form>