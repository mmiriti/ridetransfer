<html>
<body>
	<table>
		<tr>
			<td>Номер заказа:</td><td><?php echo $order->id; ?></td>
		</tr>
		<tr>
			<td>Клиент:</td><td><?php echo $order->client->title; ?></td>
		</tr>
		<tr>
			<td>Дата и время создания:</td><td><?php echo date("d.m.Y H:i", strtotime($order->create_time)); ?></td>
		</tr>
		<tr>
			<td>Дата и время подачи автомобиля:</td><td><?php echo date("d.m.Y H:i", strtotime($order->ride_time)); ?></div></td>
		</tr>
		<tr>
			<td>Пассажир:</td><td><?php echo $order->passenger->fullname; ?></td>
		</tr>	
		<tr>
			<td>Номер телефона пассажира:</td><td><?php echo $order->passenger->phone; ?></td>
		</tr>
		<tr>
			<td>Примечание:</td><td><?php echo $order->comment; ?></td>
		</tr>
		<tr>
			<td>Пункт отправления:</td><td><?php echo $order->route->locationFrom->title; ?></td>
		</tr>
		<tr>
			<td><?php echo $order->route->locationFrom->locationType->typeInfo->title; ?>:</td><td><?php echo $order->info_from; ?></td>
		</tr>
		<tr>
			<td>Пункт назначения:</td><td><?php echo $order->route->locationTo->title; ?></td>
		</tr>
		<tr>
			<td><?php echo $order->route->locationTo->locationType->typeInfo->title; ?>:</td><td><?php echo $order->info_to; ?></td>
		</tr>
		<tr>
			<td>Класс автомобиля:</td><td><?php echo $order->route->autoClass->title; ?></td>
		</tr>
		<tr>
			<td>Количество пассажиров:</td><td><?php echo $order->passanger_count; ?></td>
		</tr>
		<tr>
			<td>Надпись на табличке:</td><td><?php echo $order->sign_text; ?></td>
		</tr>
		<tr>
			<td>Стоимость:</td><td><?php echo $order->price; ?></td>
		</tr>
		<tr>
			<td>Статус заказа:</td><td><?php echo $order->orderStatus->title; ?></td>
		</tr>
	</table>
</body>
</html>