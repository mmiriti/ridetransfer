<?php
function generateInput($model, $fieldName, $fieldTypes) {
	$fieldID = get_class($model) . '_' . $fieldName;
	$inputName = get_class($model) . '[' . $fieldName . ']';
	$attributeLabels = $model->attributeLabels();
	$fieldLabel = $attributeLabels[$fieldName];

	if(array_key_exists($fieldName, $fieldTypes)) {
		$fieldType = $fieldTypes[$fieldName];
		if(is_array($fieldType))
		{
			if(isset($fieldType['listData']))
			{
				$listData = $fieldType['listData'];
			}

			$fieldType = $fieldType['type'];
		}
	}else{
		$fieldType = 'text';
	}

	$errors = $model->getErrors($fieldName);

	?>
	<div class="form-group<?php if($errors) echo ' has-error'; ?>">
		<label for="<?php echo $fieldID; ?>" class="col-sm-2 control-label"><?php echo $fieldLabel; ?></label>
		<div class="col-sm-10">
	<?php

	$htmlOptions = array('class' => 'form-control', 'id' => $fieldID);

	switch ($fieldType) 
	{
		case 'text':
			echo CHtml::textField($inputName, $model[$fieldName], $htmlOptions);
		break;

		case 'password':
			echo CHtml::passwordField($inputName, $model[$fieldName], $htmlOptions);
		break;

		case 'date':			
			echo CHtml::dateField($inputName, $model[$fieldName], $htmlOptions);
		break;

		case 'date_time':
			echo CHtml::dateField($inputName . '[date]', date("Y-m-d", strtotime($model[$fieldName])), $htmlOptions);
			echo CHtml::timeField($inputName .'[time]', date("H:i:s", strtotime($model[$fieldName])), $htmlOptions);
		break;

		case 'textarea':
			echo CHtml::textArea($inputName, $model[$fieldName], array_merge($htmlOptions, array('rows' => 3)));
		break;

		case 'select':
			echo CHtml::dropDownList($inputName, $model[$fieldName], $listData, $htmlOptions);
		break;
	}

	$errors = $model->getErrors($fieldName);

	if(count($errors)) {
		foreach ($errors as $error) {
			?>
			<p class="help-block"><?php echo $error; ?></p>
			<?php
		}
	}

	?>
		</div>
	</div>
	<?php
}
?>
<form class="form-horizontal" role="form" action="" method="post">
	<?php 
	foreach ($this->model->attributeLabels() as $fieldName => $fieldLabel) 
	{ 
		if(!in_array($fieldName, $this->excludeFields))
		{
			generateInput($this->model, $fieldName, $this->fieldTypes);
		}
	}
	?>
	<button type="submit" class="btn btn-primary">Сохранить</button>
	<button type="reset" class="btn btn-default">Сбросить</button>
</form>