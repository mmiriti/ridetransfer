<div class="row">
	<div class="col-md-4 col-md-offset-8">
		<table class="table table-hover">
			<thead>
				<tr>
					<th colspan="2">Итоги:</th>
				</tr>				
			</thead>
			<tbody>
				<tr>
					<td>Всего заказов:</td>
					<td><?php echo count($this->list); ?></td>
				</tr>
				<tr>
					<td>Сумма:</td>
					<td><?php echo $this->price_sum; ?>р.</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>