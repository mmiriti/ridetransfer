<?php

/**
* ListSummary
*/
class ListSummary extends CWidget
{
	public $list;
	public $price_sum;

	function run()
	{
		$this->price_sum = 0;

		foreach ($this->list as $order) 
		{
			$this->price_sum += $order->price;
		}

		$this->render('listSummary');
	}
}