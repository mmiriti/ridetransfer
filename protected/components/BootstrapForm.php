<?php

/**
* BootstrapForm
*/
class BootstrapForm extends CWidget
{
	public $model;
	public $excludeFields = array('id');
	public $fieldTypes = array();

	function run()
	{
		$this->render('bootstrapForm');
	}
}