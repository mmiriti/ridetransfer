<?php

/**
* Common
*/
class Common
{
	public static function getAutoClassList($location_from, $location_to)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 'location_from = :from AND location_to = :to';
		$criteria->params[':from'] = $location_from;
		$criteria->params[':to'] = $location_to;
		$criteria->group = 'auto_class_id';

		$routes = Route::model()->findAll($criteria);
		$classes = array();

		foreach ($routes as $r) {
			$classes[$r->id] = $r->autoClass->title . ' (кол-во мест: ' . $r->autoClass->seat_count . ')';
		}

		return $classes;
	}
}