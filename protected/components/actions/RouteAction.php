<?php

/**
* Route action
*/
class RouteAction extends CAction
{
	public function run($id)
	{
		$route = Route::model()->findByPk($id);
		$response = $route->attributes;
		$response['seat_count'] = $route->autoClass->seat_count;
		echo CJSON::encode($response);
	}
}