<?php

/**
* Fetchlocations actin
*/
class FetchLocations extends CAction
{
	
	public function run($q)
	{
		if(strlen($q) >= 3)
		{
			$city_criteria = new CDbCriteria;
			$city_criteria->compare('name', $q, true);
			$citys = City::model()->findAll($city_criteria);

			$city_ids = array();

			foreach ($citys as $city) {
				$city_ids[] = $city->id;
			}

			$location_criteria = new CDbCriteria;
			$location_criteria->compare('title', $q, true);
			$location_criteria->addInCondition('city_id', $city_ids, 'OR');
			$location_criteria->limit = 10;

			$locations = Location::model()->findAll($location_criteria);

			$response = array('items' => array());

			foreach ($locations as $l) 
			{
				$response['items'][] = array('id' => $l->id, 'title' => $l->fulltitle);
			}

			echo CJSON::encode($response);
		}
	}
}