<?php

/**
* OrderListAction
*/
class OrderListAction extends CAction
{	
	public function run($id=null, $action=null, $deleted=0)
	{
		if(($action == 'create')||($action == 'update'))
		{
			$errors = array();

			if($action == 'create')
			{
				$order = new Order;

				$order->order_status_id = 1;
				$order->price = 0;

				if(Yii::app()->user->role == 'client')
				{
					$client = Client::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
					$order->client_id = $client->id;
				}

				$order->passenger = new Passenger;
				$order->ride_time = date("Y-m-d 18:00:00", time()+60*60*24);
			}

			if($action == 'update')
			{
				$order = Order::model()->findByPk($id);
			}

			if(isset($_POST['Order']))
			{
				$passenger = Passenger::model()->findByAttributes($_POST['Passenger']);

				if($passenger == null)
				{
					$passenger = new Passenger();
				}

				$passenger->attributes = $_POST['Passenger'];

				$passenger->client_id = $_POST['Order']['client_id'];				

				$ride_time = $_POST['Order']['ride_time']['date'] . ' ' . $_POST['Order']['ride_time']['time'];
				$_POST['Order']['ride_time'] = $ride_time;

				$order->attributes = $_POST['Order'];

				if($passenger->save())
				{
					$order->passenger_id = $passenger->id;

					if($order->save())
					{
						if($action=='create') {
							$mailBody = $this->getController()->renderInternal('protected/components/views/mail.php', array('order' => $order), true);
							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
							@mail(Yii::app()->params['adminEmail'], "=?utf-8?B?" . base64_encode(Yii::app()->params['orderEmailSubject']) . "?=", $mailBody, $headers);
						}

						$this->getController()->redirect(array('index'));
					}else
					{
						$errors = array_merge($errors, $order->getErrors());
					}
				}else
				{
					$errors = array_merge($errors, $passenger->getErrors());
				}
			}

			$autoClassList = array();

			if($order->route != null)
			{
				$autoClassList = Common::getAutoClassList($order->route->location_from, $order->route->location_to);
			}

			$orderFormData = array(
				'order' => $order, 
				'autoClassList' => $autoClassList, 
				'errors' => $errors
			);

			if(isset($passenger))
			{
				$orderFormData['passenger'] = $passenger;
			}

			$this->getController()->render('_orderForm', $orderFormData);
		}else
		{
			if($action == 'delete')
			{
				$order = Order::model()->findByPk($id);
				if($order->deleted == 1)
				{
					$order->delete();
				}else
				{
					$order->deleted = 1;
					$order->save();
				}				

				$this->getController()->redirect(array('index', 'deleted' => $deleted));
			}

			if($action == 'view')
			{
				$this->getController()->renderPartial('_orderView', array('order' => Order::model()->findByPk($id)));
			}else
			{
				$criteria = new CDbCriteria;
				$criteria->order = 'create_time desc';
				$criteria->compare('deleted', $deleted);

				if(Yii::app()->user->role == 'client')
				{
					$client = Client::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
					$criteria->compare('client_id', $client->id);
				}

				$this->getController()->render('index', array(
					'ordersList' => Order::model()->findAll($criteria)
				));
			}
		}
	}
}