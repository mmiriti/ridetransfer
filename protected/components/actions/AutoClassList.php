<?php

/**
* 
*/
class AutoClassList extends CAction
{
	public function run($from, $to)
	{
		$response = array();
		$response['classes'] = Common::getAutoClassList($from, $to);
		$response['to_type'] = Location::model()->findByPk($to)->locationType->typeInfo->attributes;
		echo CJSON::encode($response);
	}
}