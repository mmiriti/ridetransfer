<?php

/**
* FindRoute action
*/
class FindRoute extends CAction
{
	
	public function run($location_from)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "location_from = :location_from_id";
		$criteria->params[':location_from_id'] = $location_from;
		$criteria->group = 'location_to';

		$routes = Route::model()->findAll($criteria);

		$response = array(
			'list_to' => array(),
			'from_type' => Location::model()->findByPk($location_from)->locationType->typeInfo->attributes
			);

		if($routes != null)
		{
			foreach ($routes as $r) 
			{
				$response['list_to'][$r->location_to] = $r->locationTo->title . ' (' . $r->locationTo->locationType->name . ')';
			}
		}

		echo CJSON::encode($response);
	}
}