<?php

class AdminController extends Controller
{
	public $layout='//layouts/admin';

	public function actions()
	{
		return array(
			'index' => 'application.components.actions.OrderListAction',
			'autoclass'=>'application.components.actions.AutoClassList',
			'route' => 'application.components.actions.RouteAction',
			'findroute' => 'application.components.actions.FindRoute',
			'fetchlocations' => 'application.components.actions.FetchLocations'
		);
	}

	public function actionUsers($id=null, $action=null)
	{
		if(($action == 'create')||($action == 'update'))
		{
			if($action == 'create')
				$user = new User();
			if($action == 'update')
				$user = User::model()->findByPk($id);

			if(isset($_POST['User']))
			{
				$user->attributes = $_POST['User'];
				$user->pwd_hash = md5($_POST['User']['pwd_hash']);

				if($user->save())
				{
					$this->redirect(array('users'));
				}
			}

			$this->render('_userForm', array('user' => $user));
		}else
		{
			if($action == 'delete')
			{
				if($id != Yii::app()->user->id)
				{
					User::model()->deleteByPk($id);				
				}

				$this->redirect(array('users'));
			}

			$this->render('users');
		}
	}

	public function actionClients($id=null, $action=null)
	{
		if(($action == 'create')||($action == 'update'))
		{
			if($action == 'create')
				$client = new Client();
			if($action == 'update')
				$client = Client::model()->findByPk($id);

			if(isset($_POST['Client']))
			{
				$client->attributes = $_POST['Client'];
				if($client->save())
				{
					$this->redirect(array('clients'));
				}
			}

			$this->render('_clientForm', array('client' => $client));
		}else
		{
			if($action == 'delete')
			{
				Client::model()->deleteByPk($id);
				$this->redirect(array('clients'));
			}

			$this->render('clients');
		}
	}

	public function actionPassengers($id=null, $action=null)
	{
		if(($action == 'create')||($action == 'update'))
		{
			if($action == 'create')
				$passenger = new Passenger();
			if($action == 'update')
				$passenger = Passenger::model()->findByPk($id);

			if(isset($_POST['Passenger']))
			{
				$passenger->attributes = $_POST['Passenger'];
				if($passenger->save())
				{
					$this->redirect(array('passengers'));
				}
			}

			$this->render('_passengerForm', array('passenger' => $passenger));
		}else
		{
			if($action == 'delete')
			{
				Passenger::model()->deleteByPk($id);
				$this->redirect(array('passengers'));
			}

			$this->render('passengers');		
		}
	}

	public function actionStatuses($id=null, $action=null)
	{
		if(($action == 'create')||($action == 'update'))
		{
			if($action == 'create')
				$orderstatus = new OrderStatus();
			if($action == 'update')
				$orderstatus = OrderStatus::model()->findByPk($id);

			if(isset($_POST['OrderStatus']))
			{
				$orderstatus->attributes = $_POST['OrderStatus'];
				if($orderstatus->save())
				{
					$this->redirect(array('statuses'));
				}
			}

			$this->render('_orderstatusForm', array('orderstatus' => $orderstatus));
		}else
		{
			if($action == 'delete')
			{
				OrderStatus::model()->deleteByPk($id);
				$this->redirect(array('statuses'));
			}

			$this->render('statuses');
		}
	}

	public function actionClasses($id=null, $action=null)
	{
		if(($action == 'create')||($action == 'update'))
		{
			if($action == 'create')
				$autoclass = new AutoClass();
			if($action == 'update')
				$autoclass = AutoClass::model()->findByPk($id);

			if(isset($_POST['AutoClass']))
			{
				$autoclass->attributes = $_POST['AutoClass'];
				if($autoclass->save())
				{
					$this->redirect(array('classes'));
				}
			}

			$this->render('_autoclassForm', array('autoclass' => $autoclass));
		}else
		{
			if($action == 'delete')
			{
				AutoClass::model()->deleteByPk($id);
				$this->redirect(array('classes'));
			}

			$this->render('classes');
		}
	}
}