<?php

class UserController extends Controller
{
	public function actionLogin()
	{
		if(isset($_POST['UserLogin'])) 
		{
			$userIdentity = new UserIdentity($_POST['UserLogin']['login'], $_POST['UserLogin']['password']);

			if($userIdentity->authenticate()) 
			{
				Yii::app()->user->login($userIdentity, 3600*24*7);
			
				$this->redirect(array('/' . Yii::app()->user->role));
			} else 
			{
				$this->redirect(array('site/index', 'error' => $userIdentity->errorMessage));
			}
		}
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect('/');
	}
}