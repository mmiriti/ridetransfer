<?php

class OperatorController extends Controller
{
	public $layout='//layouts/operator';
	
	public function actions()
	{
		return array(
			'index' => 'application.components.actions.OrderListAction',
			'autoclass'=>'application.components.actions.AutoClassList',
			'route' => 'application.components.actions.RouteAction',
			'findroute' => 'application.components.actions.FindRoute',
			'fetchlocations' => 'application.components.actions.FetchLocations'
		);
	}
}