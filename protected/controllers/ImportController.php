<?php

define('F_COUNTRY_ID',							0); // CountryID	
define('F_COUNTRY_NAME',						1); // Страна	

define('F_REGION_ID',								2); // RegionID	
define('F_REGION_NAME', 						3); // Регион отправления	

define('F_CITY_ID', 								4); // CityID	
define('F_CITY_NAME', 							5); // Город отправления	

define('F_FROM_LOCATION_ID',				6); // PlaceID	
define('F_FROM_LOCATION_NAME',			7); // Локация отправления	

define('F_FROM_LOCATION_TYPE_ID', 	8); // TypeID	
define('F_FROM_LOCATION_TYPE_NAME', 9); // тип локации	

define('F_TO_LOCATION_ID', 					10); // PlaceID	
define('F_TO_LOCATION_NAME', 				11); // Локация прибытия	

define('F_TO_LOCATION_TYPE_ID', 		12); // TypeID	
define('F_TO_LOCATION_TYPE_NAME', 	13); // тип локации	 

define('F_PRICE', 									14); // Price	

define('F_AUTO_CLASS_ID',						15); // CarClassID	
define('F_AUTO_CLASS_NAME', 				16); // класс авто	
define('F_AUTO_CLASS_ROOM', 				17); // Вместимость


class ImportController extends Controller
{

	private function generateLocationType($id, $title)
	{
		$result = LocationType::model()->findByPk($id);
		
		if($result == null)
		{
			$result = new LocationType();
			$result->id = $id;
			$result->name = $title;
		}

		switch ($title) 
		{
			case 'в черте города':
				$result->type_info_id = 1;
			break;
			case 'Аэропорт':
				$result->type_info_id = 2;
			break;
			case 'Населенный пункт':
				$result->type_info_id = 1;
			break;
			case 'в пределах МКАД':
				$result->type_info_id = 1;
			break;
			case 'Прочее':
				$result->type_info_id = 1;
			break;
			case 'Ж/д вокзал':
				$result->type_info_id = 3;
			break;
			case 'центр города':
				$result->type_info_id = 1;
			break;
			case 'в пределах КАД':
				$result->type_info_id = 1;
			break;
			case 'Вблизи города':
				$result->type_info_id = 1;
			break;
		}

		$result->save();
		

		return $result;
	}

	public function actionIndex()
	{
		$error = '';

		if(isset($_FILES['csv_file'])) 
		{
			if($_FILES['csv_file']['error'] == 0)
			{
				$file = fopen($_FILES['csv_file']['tmp_name'], "r");			


				while (($data = fgetcsv($file, 65000, ";")) !== FALSE) 
				{
					if(($data[F_FROM_LOCATION_ID] != 0)&&($data[F_TO_LOCATION_ID] != 0))
					{
						$transaction = Yii::app()->db->beginTransaction();

						try
						{
							if(($data[F_COUNTRY_ID] != '') && ($data[F_COUNTRY_ID] != 0))
							{
								$country = Country::model()->findByPk($data[F_COUNTRY_ID]);
								if($country == null)
								{
									$country = new Country();
									$country->id = intval($data[F_COUNTRY_ID]);
									$country->name = $data[F_COUNTRY_NAME];
									$country->save();
								}
							} else 
							{
								$country = null;
							}

							if(($data[F_REGION_ID] != '') && ($data[F_REGION_ID] != 0))
							{
								$region = Region::model()->findByPk(intval($data[F_REGION_ID]));
								if($region == null)
								{
									$region = new Region();
									$region->id = $data[F_REGION_ID];
									if($country != null)
									{
										$region->country_id = $country->id;
									}
									$region->name = $data[F_REGION_NAME];
									$region->save();
								}
							}else
							{
								$region = null;
							}

							if(($data[F_CITY_ID] != '') && ($data[F_CITY_ID] != 0))
							{
								$city = City::model()->findByPk(intval($data[F_CITY_ID]));
								if($city == null)
								{
									$city = new City();
									$city->id = intval($data[F_CITY_ID]);
									if($region != null)
									{
										$city->region_id = $region->id;
									}
									$city->name = $data[F_CITY_NAME];
									$city->save();
								}
							}else
							{
								$city = null;
							}

							if($data[F_FROM_LOCATION_TYPE_ID] != '')
							{
								$location_type = $this->generateLocationType(intval($data[F_FROM_LOCATION_TYPE_ID]), trim($data[F_FROM_LOCATION_TYPE_NAME]));
							}								

							if($data[F_FROM_LOCATION_ID] != '')
							{
								$location = Location::model()->findByPk(intval($data[F_FROM_LOCATION_ID]));
								if($location == null)
								{
									$location = new Location();
									$location->id = intval($data[F_FROM_LOCATION_ID]);
									if($city != null)
									{
										$location->city_id = $city->id;
									}
									$location->location_type_id = $location_type->id;
									$location->title = $data[F_FROM_LOCATION_NAME];
									$location->save();
								}
							}

							if($data[F_TO_LOCATION_TYPE_ID] != '')
							{
								$location_type_to = $this->generateLocationType(intval($data[F_TO_LOCATION_TYPE_ID]), trim($data[F_TO_LOCATION_TYPE_NAME]));
							}

							if($data[F_TO_LOCATION_ID] != '')
							{
								$location_to = Location::model()->findByPk(intval($data[F_TO_LOCATION_ID]));
								if($location_to == null)
								{
									$location_to = new Location();
									$location_to->id = intval($data[F_TO_LOCATION_ID]);
									if($city != null)
									{
										$location_to->city_id = $city->id;
									}
									$location_to->location_type_id = $location_type_to->id;
									$location_to->title = $data[F_TO_LOCATION_NAME];
									$location_to->save();
								}
							}

							if($data[F_AUTO_CLASS_ID] != '')
							{
								$auto_class = AutoClass::model()->findByPk(intval($data[F_AUTO_CLASS_ID]));
								if($auto_class == null)
								{
									$auto_class = new AutoClass();
									$auto_class->id = intval($data[F_AUTO_CLASS_ID]);
									$auto_class->title = $data[F_AUTO_CLASS_NAME];
									$auto_class->seat_count = intval($data[F_AUTO_CLASS_ROOM]);
									$auto_class->save();
								}
							}

							$price = $data[F_PRICE];

							$newRoute = Route::model()->findByAttributes(array(
								'location_from' => $location->id,
								'location_to' => $location_to->id,
								'auto_class_id' => $auto_class->id,
								'price' => $price
								));

							if($newRoute == null)
							{
								$newRoute = new Route();
								$newRoute->location_from = $location->id;
								$newRoute->location_to = $location_to->id;
								$newRoute->auto_class_id = $auto_class->id;
								$newRoute->price = $price;
								$newRoute->save();
							}

							$transaction->commit();
						}catch(Exception $e) {
							$error .=  $e;
							$transaction->rollback();
							break;
						}
					}
				}									
			}else{
				$error = print_r($_FILES, true);
			}
		}
		$this->render('index', array('error' => $error));
	}
}